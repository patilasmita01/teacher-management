<%@page import="java.sql.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
<!DOCTYPE html>
 <html>

        <head>
            <title>Teacher Management System</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        </head>

        <body>
            <div class="row">
                <!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

                <div class="container">
                    <h3 class="text-center">List of Teachers</h3>
                    <hr>
                    <div class="container text-center">

                        <a href="<%=request.getContextPath()%>/TeacherLoginPage.jsp" class="btn btn-primary">Add New Teacher</a>
                    </div>
                        <div class="container text-right">

                        <a href="<%=request.getContextPath()%>/LogoutAdmin.jsp" class="btn btn-danger">Logout</a>
                       
                    </div>
                    <br>
                    <table class="table table-striped">
                      
                            <tr>
                                <th>Sr No</th>
                                <th>Name</th>
                                <th>Password</th>
                                <th>Subject</th>
                                <th>StaffID</th>
                                <th>Standard</th>
                                <th>Update</th>
                                <th>Delete</th>

                            </tr>
                            
                            <%
                        		Class.forName("com.mysql.cj.jdbc.Driver");  
								Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/TeachersMang","root","root");
 								Statement statement = con.createStatement();
								ResultSet rs = statement.executeQuery("select * from usersLists");
								
								 while (rs.next()) 
					             {  
					                 int ID = rs.getInt("srNo");  
					                 String UserName = rs.getString("staffName");  
					                 String Password = rs.getString("staffPass");  
					                 String Subject = rs.getString("staffSub");   
					                 String StaffID = rs.getString("staffId");   
					                 String Standard = rs.getString("staffStd");
					                 %>	

					                <tr>
					                <td><%= rs.getInt("srNo")  %></td>		
					                <td><%= rs.getString("staffName") %></td>
					                <td><%= rs.getString("staffPass") %></td>
					                <td><%= rs.getString("staffSub") %></td>
					                <td><%= rs.getString("staffId") %></td>
					                <td><%= rs.getString("staffStd") %></td>
					               	<td><a href="<%=request.getContextPath()%>/EditTeacher.jsp?id=<%=rs.getInt("staffId")%>">Edit</a></td>
					                <td><a href="<%= request.getContextPath()%>/DeleteTeacherServlet.jsp?id=<%= rs.getInt("staffId")%>">Delete</a></td>
					                </tr>
					           <% 
					             }  
					               %>
                    </table>
                </div>
            </div>
        </body>

        </html>