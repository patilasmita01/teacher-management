
public class TeachersList {
	
	int staffId;
	String staffName;
	String staffPass;
	String staffSub;
	int staffStd;
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffPass() {
		return staffPass;
	}
	public void setStaffPass(String staffPass) {
		this.staffPass = staffPass;
	}
	public String getStaffSub() {
		return staffSub;
	}
	public void setStaffSub(String staffSub) {
		this.staffSub = staffSub;
	}
	public int getStaffStd() {
		return staffStd;
	}
	public void setStaffStd(int staffStd) {
		this.staffStd = staffStd;
	}
	
	
}
