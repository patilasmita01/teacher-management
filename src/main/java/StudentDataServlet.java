

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StudentDataServlet
 */
@WebServlet("/StudentDataServlet")
public class StudentDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentDataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out = response.getWriter();  
       
        try
        {
        	Class.forName("com.mysql.cj.jdbc.Driver");  
        	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/TeachersMang","root","root");
        	Statement statement = con.createStatement();
        	PreparedStatement pstmt = con.prepareStatement("insert into studentsData(studName, studRollNo, studAge) values(?,?,?);");				
        	
        		pstmt.setString(1, request.getParameter("studName"));
        		pstmt.setString(2,request.getParameter("studRollNo"));
        		pstmt.setString(3,request.getParameter("studAge"));
        		
        		
        		pstmt.executeUpdate();
        		
        		con.close(); 
        	
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
        response.sendRedirect("TeacherHomePage.jsp");
        
        out.close();

	}

}
