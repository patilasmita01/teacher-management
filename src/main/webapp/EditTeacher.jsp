<%@page import="java.sql.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
 <head>
    <meta charset="UTF-8">
    	<link rel="stylesheet" href="style.css">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <style>
		  *{
		  margin: 0;
		  padding: 0;
		  box-sizing: border-box;
		}
		body{
		  height: 740px;
		  display: flex;
		  justify-content: center;
		  align-items: center;
		  padding: 10px;
		  background: linear-gradient(135deg, #71b7e6, #9b59b6);
		}
		.container{
		  max-width: 700px;
		  width: 100%;
		  background-color: #fff;
		  padding: 25px 30px;
		  border-radius: 5px;
		  box-shadow: 0 5px 10px rgba(0,0,0,0.15);
		}
		.container .title{
		  font-size: 25px;
		  font-weight: 500;
		  position: relative;
		}
		
		.content form .user-details{
		  display: flex;
		  flex-wrap: wrap;
		  justify-content: space-between;
		  margin: 20px 0 12px 0;
		}
		form .user-details .input-box{
		  margin-bottom: 15px;
		  width: calc(100% / 2 - 20px);
		}
		form .input-box span.details{
		  display: block;
		  font-weight: 500;
		  margin-bottom: 5px;
		}
		.user-details .input-box input{
		  height: 45px;
		  width: 100%;
		  outline: none;
		  font-size: 16px;
		  border-radius: 5px;
		  padding-left: 15px;
		  border: 1px solid #ccc;
		  border-bottom-width: 2px;
		  
		}
		.user-details .input-box input:focus,
		.user-details .input-box input:valid{
		  border-color: #9b59b6;
		}
		
		 form .button{
		   height: 45px;
		   margin: 35px 0
		 }
		 form .button input{
		   height: 100%;
		   width: 100%;
		   border-radius: 5px;
		   border: none;
		   color: #fff;
		   font-size: 18px;
		   font-weight: 500;
		   letter-spacing: 1px;
		   cursor: pointer;
		   transition: all 0.3s ease;
		   background: linear-gradient(135deg, #71b7e6, #9b59b6);
		 }
		 form .button input:hover{
		
		  background: linear-gradient(-135deg, #71b7e6, #9b59b6);
		  }
		 @media(max-width: 584px){
		 .container{
		  max-width: 100%;
		}
		
		 
	
		  }
		  @media(max-width: 459px){
		  .container .content .category{
		    flex-direction: column;
		  }
		}
		     	
     
     </style>
     
 </head>
<body>
  <div class="container">
    <div class="title">Registration</div>
    <div class="content">
      <form action="UpdateTeacherServlet" method="post">
      	<label><%=request.getParameter("id")%></label>
		<input type="hidden" name = "id" value = "<%=request.getParameter("id")%>">
        <div class="user-details">
          <div class="input-box">
            <span class="details">Username</span>
            <input type="text" placeholder="Enter your name" required name= "staffName">
          </div>
          <div class="input-box">
            <span class="details">Password</span>
            <input type="text" placeholder="Enter your password" required name= "staffPass">
          </div>
          <div class="input-box">
            <span class="details">Subject</span>
            <input type="text" placeholder="Enter your subject" required name = "staffSub">
          </div>
          <div class="input-box">
            <span class="details">Standard</span>
            <input type="number" placeholder="Enter standard" required name = "staffStd">
          </div>
          <div class="input-box">
            <span class="details">Teacher Id</span>
            <input type="number" placeholder="Enter your id" required name = "staffId">
          </div>
          
         
     
        </div>
        <div class="button">
          <input type="submit" value="Register" name = "btnRegister">
        </div>
      </form>
    </div>
  </div>	

</body>
</html>