<%@page import="java.sql.*" %>
<%@ page import ="java.util.List"%>
<%@ page import ="java.util.ArrayList"%>
<%@ page import ="java.io.IOException"  %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
 <head>
            <title>Teacher Management System</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        </head>

        <body>
            <div class="row">
                <!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

                <div class="container">
                    <h3 class="text-center">List of Students</h3>
                    <hr>
                    <div class="container text-center">

                        <a href="<%=request.getContextPath()%>/StudentDataInsert.jsp" class="btn btn-primary">Add New Student</a>
                       
                    </div>
                    <div class="container text-right">

                        <a href="<%=request.getContextPath()%>/LogoutTeacher.jsp" class="btn btn-danger">Logout</a>
                       
                    </div>
                    <br>
                    <table class="table table-striped">
                      
                            <tr>
                                <th>Sr No</th>
                                <th>Name</th>
                                <th>Roll No</th>
                                <th>Age</th>
                                <th>Update</th>
                                <th>Delete</th>

                            </tr>
 							<%
                            
                        		Class.forName("com.mysql.cj.jdbc.Driver");  
								Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/TeachersMang","root","root");
 								Statement statement = con.createStatement();
								ResultSet rs = statement.executeQuery("select * from studentsData");
								
								 while (rs.next()) 
					             {  
					                 int srno = rs.getInt("srNo");  
					                 String studname = rs.getString("studName");  
					                 String studRoll = rs.getString("studRollNo");  
					                 String studage = rs.getString("studAge");   
					                
					                 %>	

					                <tr>
					                <td><%= rs.getInt("srNo")  %></td>		
					                <td><%= rs.getString("studName") %></td>
					                <td><%= rs.getString("studRollNo") %></td>
					                <td><%= rs.getString("studAge") %></td>
					               	<td><a href="<%=request.getContextPath()%>/EditStudent.jsp?id=<%=rs.getInt("srNo")%>">Edit</a></td>
					                <td><a href="<%= request.getContextPath()%>/DeleteStudent.jsp?id=<%= rs.getInt("srNo")%>">Delete</a></td>
					                </tr>
					           <% 
					             }  
					               %>
                    </table>
                </div>
            </div>
        </body>
</html>